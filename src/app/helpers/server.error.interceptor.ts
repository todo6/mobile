import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, tap } from 'rxjs/operators';
import { ToastController } from '@ionic/angular';

@Injectable()
export class ServerErrorInterceptor implements HttpInterceptor {
  constructor(
    private toastController: ToastController
  ) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(async (event: HttpEvent<any>): Promise<any> => {
        const { method } = request;
        if (event instanceof HttpResponse) {
          if (event.body && event.body.message) {
            const { message } = event.body;
            console.log('toast success');
            const toast = await this.toastController.create({
              message,
              duration: 3000,
              position: 'top'
            });
            toast.present();
          }
        }
      }),
      retry(1),
      catchError(async (error: HttpErrorResponse): Promise<any> => {
        const { statusCode } = error.error;
        console.log('server error');
        const message = error.error.message || error.message;
        const toast = await this.toastController.create({
          message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
        // if (statusCode === 400 || statusCode === 422) {
        //   this.toastrService.warning(message, 'Solicitud incorrecta');
        //   // console.log(error.error.validationErrors);
        // }
        // if (statusCode === 404) {
        //   this.toastrService.warning(message, 'No encontrado');
        // }
        // if (statusCode === 409) {
        //   this.toastrService.warning(message, 'Problema');
        // }
        // if (statusCode === 403) {
        //   this.toastrService.warning(message, 'Prohibido');
        // }
        // if (statusCode === 401) {
        //   this.toastrService.warning(message, 'Sin autorización');
        // }
        // if (statusCode === 410) {
        //   this.toastrService.warning(message, 'Registro eliminado');
        // }

        // if (statusCode >= 500) {
        //   this.toastrService.error(message, 'Error');
        // }
        return throwError(message);
      }),
    );
  }
}
