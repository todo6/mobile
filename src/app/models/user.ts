import { Task } from './task';
export class User {
  _id?: string;
  name?: string;
  lastName?: string;
  email?: string;
  password?: string;
  enabled?: boolean;
  confirmed?: boolean;
  checklist?: [Task];
}
