import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChecklistFinalizedPage } from './checklist-finalized.page';

const routes: Routes = [
  {
    path: '',
    component: ChecklistFinalizedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChecklistFinalizedPageRoutingModule {}
