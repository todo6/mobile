import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChecklistFinalizedPageRoutingModule } from './checklist-finalized-routing.module';

import { ChecklistFinalizedPage } from './checklist-finalized.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChecklistFinalizedPageRoutingModule
  ],
  declarations: [ChecklistFinalizedPage]
})
export class ChecklistFinalizedPageModule {}
