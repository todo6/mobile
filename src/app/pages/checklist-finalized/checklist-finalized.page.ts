import { Component, OnInit } from '@angular/core';
import { ChecklistService } from 'src/app/services/checklist.service';
import { NavController, NavParams } from '@ionic/angular';
import { Task } from '../../models';
import { AuthenticationService } from '../../services';
@Component({
  selector: 'app-checklist-finalized',
  templateUrl: './checklist-finalized.page.html',
  styleUrls: ['./checklist-finalized.page.scss'],
})
export class ChecklistFinalizedPage implements OnInit {
  checklist: Task[];
  constructor(
    private checklistService: ChecklistService,
    private authenticationService: AuthenticationService,
    private navController: NavController,

  ) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    this.checklistService.getFinalizedChecklist().subscribe((checklist) => this.checklist = checklist);
  }

  btnDeleteAll_click(): void {
    this.checklistService.deleteChecklistByCheck(true).subscribe(() => {
      this.checklist = [];
    });
  }

  btnDelete_click(task: Task): void {
    this.checklistService.deleteChecklist(task._id).subscribe(() => {
      const index = this.checklist.indexOf(task);
      this.checklist.splice(index, 1);
    });
  }

  btnLogout_click(): void {
    this.authenticationService.logout();
    this.navController.navigateRoot('auth/login');
  }
}
