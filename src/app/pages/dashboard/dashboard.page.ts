import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { Task } from '../../models';
import { AuthenticationService } from '../../services';
import { ChecklistService } from '../../services/checklist.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  checklist;
  constructor(
    private navController: NavController,
    private checklistService: ChecklistService,
    private authenticationService: AuthenticationService,
  ) {
  }

  ngOnInit() {
  }
  ionViewDidEnter(){
    this.checklistService.getPendingChecklist().subscribe((checklist) => this.checklist = checklist);
  }

  btnLogout_click(): void {
    this.authenticationService.logout();
    this.navController.navigateRoot('auth/login');
  }

  btnShowNewTaskModal_click(): void {
    this.navController.navigateForward('task');
  }

  btnEditTaskModal_click(task: Task): void {
    this.navController.navigateForward('task', { queryParams: task});
  }

  btnChecked_click(task: Task): void {
    const editTask: Task = {
      _id: task._id,
      check: true,
    };
    this.checklistService.editTask(editTask).subscribe((taskEdited) => {
      const index = this.checklist.indexOf(task);
      this.checklist.splice(index, 1);
    });
  }

  btnDelete_click(task: Task): void {
    this.checklistService.deleteChecklist(task._id).subscribe(() => {
      const index = this.checklist.indexOf(task);
      this.checklist.splice(index, 1);
    });
  }
}
