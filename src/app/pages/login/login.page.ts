import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NavController, ToastController } from '@ionic/angular';
import { Session } from '../../models';
import { AuthenticationService } from '../../services';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email;
  password;
  constructor(
    private navController: NavController,
    private authenticationService: AuthenticationService,
    private toastController: ToastController,
  ) { }

  ngOnInit() {
    const currentUser: Session = this.authenticationService.currentUserValue;
    if (currentUser?.token) {
      this.navController.navigateForward('dashboard');
    }
  }

  async btnLogin_click(form: FormGroup): Promise<void> {

    const email = form.controls.email.value;
    const password = form.controls.password.value;
    this.authenticationService.login(email, password).pipe(first())
    .subscribe((data) => {
      this.navController.navigateForward('dashboard');
    });
  }


}
