import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { NavController } from '@ionic/angular';
import { User } from '../../models';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  name;
  lastName;
  email;
  password;

  constructor(
    private userService: UserService,
    private navController: NavController
  ) { }

  ngOnInit() {
  }

  btnSignUp_click(form: FormGroup) {
    const name = form.controls.name.value;
    const lastName = form.controls.lastName.value;
    const email = form.controls.email.value;
    const password = form.controls.password.value;
    const user: User = {
      password,
      email,
      name,
      lastName,
    };
    this.userService
      .signUp(user)
      .pipe(first())
      .subscribe(() => {
        this.navController.navigateRoot('auth/login');
      });
  }

}
