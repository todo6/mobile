import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Task } from '../../models';
import { ChecklistService } from '../../services/checklist.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {
  task: Task;
  txtTitle;
  txtDescription;
  constructor(
    private checklistService: ChecklistService,
    private navController: NavController,
    private route: ActivatedRoute,


  ) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      console.log({params});
      this.task = {
        _id: params._id || undefined,
        title: params.title || '',
        description: params.description || '',
      };
      this.txtTitle = params.title;
      this.txtDescription = params.description;
    });

  }


  btnSaveTask_click(form: FormGroup): void {
    const title = form.controls.title.value;
    const description = form.controls.description.value;
    this.task.title = title;
    this.task.description = description;
    if (this.task._id) {
      this.checklistService.editTask(this.task).subscribe((responseTask) => {
        return this.navController.navigateRoot('dashboard');
      });
    } else {
      this.checklistService.addTask(this.task).subscribe((responseTask) => {
        return this.navController.navigateRoot('dashboard');
      });
    }
  }
}
