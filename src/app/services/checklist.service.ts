import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Task } from '../models';
import { environment } from '../../environments/environment';

const { host }: { host: string } = environment;

interface ResponseChecklist {
  message: string;
  task: Task;
}
@Injectable({ providedIn: 'root' })
export class ChecklistService {
  constructor(private http: HttpClient) {}

  public getPendingChecklist(): Observable<Task[]> {
    return this.http.get<Task[]>(`${host}/users/checklist?check=false`);
  }

  public getFinalizedChecklist(): Observable<Task[]> {
    return this.http.get<Task[]>(`${host}/users/checklist?check=true`);
  }

  public addTask(checklist: Task): Observable<ResponseChecklist> {
    return this.http.post<ResponseChecklist>(`${host}/users/checklist`, checklist);
  }

  public editTask(task: Task): Observable<ResponseChecklist> {
    const { title, description, check } = task;
    return this.http.patch<ResponseChecklist>(`${host}/users/checklist/${task._id}`, {
      title,
      description,
      check,
    });
  }

  public deleteChecklist(_id: string): Observable<{ message: string }> {
    return this.http.delete<{ message: string }>(`${host}/users/checklist/${_id}`);
  }

  public deleteChecklistByCheck(check: boolean): any {
    console.log('test check', check);
    // return this.http.delete<any>(`${host}/users/checklist`);
    return this.http.delete<{ message: string }>(`${host}/users/checklist/check/${check}`);
  }
}
